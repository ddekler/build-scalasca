#!/bin/bash

if [[ $# -eq 1 ]]; then
    PREFIX=$1
    if [[ ! -d $PREFIX ]]; then
        echo "prefix directory does not exist: $PREFIX"
        exit 1
    fi
else
    PREFIX=/home/$USER/.local
fi


#if [[ -z $(which cubew-config) ]];
#then
    if [[ ! -f 'cubew-4.8.2.tar.gz' ]];
    then
        wget 'https://apps.fz-juelich.de/scalasca/releases/cube/4.8/dist/cubew-4.8.2.tar.gz'
	tar -xf cubew-4.8.2.tar.gz
    fi
    cd cubew-4.8.2
    ./configure --prefix=$PREFIX
    make -j 20
    make install
#fi

#if [[ -z $(which cubelib-config) ]];
#then
    if [[ ! -f 'cubelib-4.8.2.tar.gz' ]];
    then
        wget 'https://apps.fz-juelich.de/scalasca/releases/cube/4.8/dist/cubelib-4.8.2.tar.gz'
        tar -xf cubelib-4.8.2.tar.gz
    fi
    cd cubelib-4.8.2
    ./configure --prefix=$PREFIX
    make -j 20
    make install
#fi

if [[ -z $(which cube) ]]; 
then
    if [[ ! -f 'cubegui-4.8.2.tar.gz' ]];
    then
        wget 'https://apps.fz-juelich.de/scalasca/releases/cube/4.8/dist/cubegui-4.8.2.tar.gz'
        tar -xf cubegui-4.8.2.tar.gz
    fi
    cd cubegui-4.8.2
    ./configure --prefix=$PREFIX
    make -j 20
    make install
fi

