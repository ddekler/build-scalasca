# Build Scalasca

A set of scripts to build the [Scalasca](https://www.scalasca.org) tools on the School of GeoSciences machines to a user's home directory.

## Requirements

### Compile
To build Cube on Ubuntu Jammy, the `qt6-base-dev` and `mesa-common-dev` packages are needed:
```bash
apt install qt6-base-dev mesa-common-dev
```

### Runtime

To run `cube` on Ubuntu Jammy, the following packages need to be installed
 * libqt6widgets6
 * libqt6network6
 * libqt6concurrent6
 * qt6-qpa-plugins

Run 
```bash
apt install libqt6widgets6 libqt6network6 libqt6concurrent6  qt6-qpa-plugins
```

## Usage

The following will clone this repository and run scripts that will download, compile and install [scorep](https://www.vi-hps.org/projects/score-p/) (the profiler) and [CubeW](https://www.scalasca.org/software/cube-4.x/download.html) (the visualisation tool) to
`~/.local`.
Build 
```bash
cd /scratch/$USER/
git clone https://git.ecdf.ed.ac.uk/ddekler/build-scalasca
cd build-scalasca
./build-opari2.sh
./build-scorep.sh
./build-cubew.sh
```

Make sure `~/.local/bin` is in the `PATH` environment variable:
```bash
export PATH=/home/$USER/.local/bin:$PATH
```
This asignment is temporary and only lasts while the current terminal is active. To make it persistent, run
```bash
echo 'export PATH=/home/$USER/.local/bin:$PATH' >> ~/.bashrc
```

## Licence

This project is licensed under the terms of the MIT license. See [LICENCE](LICENCE) for details.
