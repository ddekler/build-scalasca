#!/bin/bash

OPARI2_VERSION="2.0.8"

if [[ $# -eq 1 ]]; then
    PREFIX=$1
    if [[ ! -d $PREFIX ]]; then
        echo "prefix directory does not exist: $PREFIX"
        exit 1
    fi
else
    PREFIX=/home/$USER/.local
fi

SRC_DIR="opari2-${OPARI2_VERSION}"
TAR_FILE="opari2-${OPARI2_VERSION}.tar.gz"
TAR_URL="https://perftools.pages.jsc.fz-juelich.de/cicd/opari2/tags/opari2-${OPARI2_VERSION}/$TAR_FILE"

#if [[ -z $(which scorep) ]]; 
#then
    if [[ ! -f $TAR_FILE ]];
    then
        wget $TAR_URL
    fi
    if [[ ! -d $SRC_DIR ]]; 
    then
        tar -xf $TAR_FILE
    fi
    if [[ -d $SRC_DIR/_build ]]; then
        echo "Removing previous _build"
        yes | rm -r $SRC_DIR/_build
    fi
    mkdir $SRC_DIR/_build
    cd $SRC_DIR/_build
    ../configure --prefix=$PREFIX --enable-shared
    make -j 20
    make install
#fi
