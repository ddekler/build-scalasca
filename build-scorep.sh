#!/bin/bash

SCOREP_VERSION="8.4"

DIR_NAME="scorep-${SCOREP_VERSION}"
TAR_NAME="${DIR_NAME}.tar.gz"
URL="https://perftools.pages.jsc.fz-juelich.de/cicd/scorep/tags/scorep-${SCOREP_VERSION}/${TAR_NAME}"

if [[ $# -eq 1 ]]; then
    PREFIX=$1
    if [[ ! -d $PREFIX ]]; then
        echo "prefix directory does not exist: $PREFIX"
        exit 1
    fi
else
    PREFIX=/home/$USER/.local
fi

#if [[ -z $(which scorep) ]]; 
#then
    if [[ ! -f ${TAR_NAME} ]];
    then
        wget ${URL}
    fi
    if [[ ! -d ${DIR_NAME} ]]; 
    then
        tar -xf ${TAR_NAME}
    fi
    if [[ -d ${DIR_NAME}/_build ]]; then
        echo "Removing previous build directory (${DIR_NAME}/_build)"
        rm -rf ${DIR_NAME}/_build
    fi
    mkdir ${DIR_NAME}/_build
    cd ${DIR_NAME}/_build
    OPARI2_CONFIG_PATH=$(which opari-config)
    ../configure --prefix=$PREFIX --with-shmem=openmpi --with-mpi=openmpi --with-libbfd=download --enable-shared --with-opari="${OPARI2_CONFIG_PATH}"
    make -j
    make install
#fi
